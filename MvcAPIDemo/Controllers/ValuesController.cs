﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MvcAPIDemo.Models;

namespace MvcAPIDemo.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2","itcast.cn" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value"+id;
        }

        //Post：一般用于添加数据。
        // POST api/values
        public UserInfo Post([FromBody]UserInfo userInfo)
        {
            userInfo.Age += 1;
            //访问数据库，然后做一个insert
            return userInfo;
        }

        //一般用作修改
        // PUT api/values/5
        public UserInfo Put(int id, [FromBody]UserInfo userInfo)
        {
            userInfo.Age = userInfo.Id + userInfo.Age;
            return userInfo;
        }

        // DELETE api/values/5
        public int Delete(int id)
        {
            return id + 2;
        }
    }
}