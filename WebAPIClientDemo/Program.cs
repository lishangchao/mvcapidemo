﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPIClientDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            RestClient client = new RestClient("http://localhost:50168");

            #region Get 方式请求列表
            string str = client.Get("api/values");

            Console.WriteLine(str); 
            #endregion

            #region Get 方式请求id对应的数据
            string strGetById = client.Get("api/values/2");

            Console.WriteLine(strGetById); 
            #endregion

            #region Post 方式 添加数据

            string postUri = "api/values/";

            string userJson = @"{""Id"":123,""Age"":12,""UserInfo"":""111""}";

            string postResponse = client.Post(userJson, postUri);

            Console.WriteLine(postResponse);
            #endregion

            #region Delete 

            string deleteUri = "api/values/3";
             string deleteResponse = client.Delete(deleteUri);

            Console.WriteLine(  deleteResponse);
            #endregion

            #region Put
            string putUri = "api/values/123";

            string userJson3 = @"{""Id"":123,""Age"":12,""UserInfo"":""111""}";

            string putResponse = client.Post(userJson3, putUri);

            Console.WriteLine(putResponse);
            #endregion

            Console.ReadKey();
        }
    }
}
